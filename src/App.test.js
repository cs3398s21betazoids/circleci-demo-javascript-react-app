import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// change comment to create a build

test('renders without crashing', () => {
  // create the html container
  const div = document.createElement('div');
  // create a react element in the container 'div'
  const createdComponent = ReactDOM.render(<App />, div);
  expect(createdComponent).toBeTruthy();
  // destroy the container
  const componentIsDeleted = ReactDOM.unmountComponentAtNode(div);
  expect(componentIsDeleted).toBeTruthy();
  
});

test('renders, tests title and does not crash', () => {
  // create the html container
  const div = document.createElement('div');
  // create a react element in the container 'div'
  const createdComponent = ReactDOM.render(<App />, div);
  expect(createdComponent).toBeTruthy();
  const headertitle = div.getElementsByTagName("a")[0];
  expect(headertitle.textContent).toBe("Baby Hippo Gram");
  // destroy the container
  const componentIsDeleted = ReactDOM.unmountComponentAtNode(div);
  expect(componentIsDeleted).toBeTruthy();
  
});
/*
test('renders, tests caption and does not crash', () => {
  // create the html container
  const div = document.createElement('div');
  // create a react element in the container 'div'
  const createdComponent = ReactDOM.render(<App />, div);
  expect(createdComponent).toBeTruthy();

  const captionText = div.getElementsByTagName("p")[0];
  expect(captionText.textContent).toBe("Teds Test");
  // destroy the container
  const componentIsDeleted = ReactDOM.unmountComponentAtNode(div);
  expect(componentIsDeleted).toBeTruthy();
  
});

test('renders test html, tests for title  without crashing', () => {
  const div = document.createElement('div');
  // create a react element in the container 'div'
  const createdComponent = ReactDOM.render(<title>Teds</title>, div);
  // Is component non-null? 
  expect(createdComponent).toBeTruthy();
  const pagetitle = div.getElementsByTagName("title")[0];
  expect(pagetitle.textContent).toBe("Teds");
  ReactDOM.unmountComponentAtNode(div);
 ;
});
*/
